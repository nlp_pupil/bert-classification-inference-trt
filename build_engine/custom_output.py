import tensorrt as trt
import numpy as np


def naive_output(config, init_dict, network, input_tensor, **kwargs):
    """
    Create the custom output
    """

    idims = input_tensor.shape
    assert len(idims) == 5
    S, B, hidden_size, _, _ = idims

    OUT = network.add_shuffle(input_tensor)
    OUT.second_transpose = (1, 0, 2, 3, 4)
    return OUT


def gap1d_output(config, init_dict, network, input_tensor, **kwargs):
    """
    Create the global average pooling 1d output
    """

    idims = input_tensor.shape
    assert len(idims) == 5
    S, B, hidden_size, _, _ = idims

    averaged = _gap1d(config, init_dict, network, input_tensor, **kwargs)

    OUT = network.add_shuffle(averaged.get_output(0))
    OUT.second_transpose = (1, 0, 2, 3, 4)
    return OUT


def gap1d_dr_output(config, init_dict, network, input_tensor, **kwargs):
    """
    Create the global average pooling 1d with dimension reduction output
    """

    idims = input_tensor.shape
    assert len(idims) == 5
    S, B, hidden_size, _, _ = idims

    averaged = _gap1d(config, init_dict, network, input_tensor, **kwargs)

    W_out = init_dict['output_weights']
    B_out = init_dict['output_bias']

    dense = network.add_fully_connected(averaged.get_output(0), B_out.size, W_out, B_out)

    OUT = network.add_shuffle(dense.get_output(0))
    OUT.second_transpose = (1, 0, 2, 3, 4)
    return OUT


def _gap1d(config, init_dict, network, input_tensor, **kwargs):
    """
    Create the global average pooling 1d operation
    """

    idims = input_tensor.shape
    assert len(idims) == 5
    S, B, hidden_size, _, _ = idims

    to_average = network.add_shuffle(input_tensor)
    to_average.second_transpose = (2, 1, 0, 3, 4)  # hidden_size, B, S, _, _

    W = trt.Weights(np.ones((1, S), dtype=np.float32) / S)
    gap1d = network.add_fully_connected(to_average.get_output(0), 1, W)  # hidden_size, B, 1, _, _

    OUT = network.add_shuffle(gap1d.get_output(0))
    OUT.second_transpose = (2, 1, 0, 3, 4)  # 1, B, hidden_size, _, _
    return OUT


def classfier_output(config, init_dict, network, input_tensor, **kwargs):
    """
    Create the classfier over sequence output
    """

    idims = input_tensor.shape
    assert len(idims) == 5
    S, B, hidden_size, _, _ = idims

    W_out = init_dict['output_weights']
    B_out = init_dict['output_bias']

    dense = network.add_fully_connected(input_tensor, B_out.size, W_out, B_out)

    OUT = network.add_shuffle(dense.get_output(0))
    OUT.second_transpose = (1, 0, 2, 3, 4)
    return OUT


def dump_3_layer_classfier_output(config, init_dict, network, input_tensor, **kwargs):
    """
    Create the classfier over sequence output
    """

    idims = input_tensor.shape
    assert len(idims) == 5
    S, B, hidden_size, _, _ = idims

    W_out1 = init_dict['output_weights1']
    B_out1 = init_dict['output_bias1']

    dense1 = network.add_fully_connected(input_tensor, B_out1.size, W_out1, B_out1)

    W_out2 = init_dict['output_weights2']
    B_out2 = init_dict['output_bias2']

    dense2 = network.add_fully_connected(dense1.get_output(0), B_out2.size, W_out2, B_out2)

    W_out3 = init_dict['output_weights3']
    B_out3 = init_dict['output_bias3']

    dense3 = network.add_fully_connected(dense2.get_output(0), B_out3.size, W_out3, B_out3)

    OUT = network.add_shuffle(dense3.get_output(0))
    OUT.second_transpose = (1, 0, 2, 3, 4)
    return OUT


def temp_classfier_output(config, init_dict, network, input_tensor, **kwargs):
    """
    Create temp classfier over sequence output
    """

    idims = input_tensor.shape
    assert len(idims) == 5
    S, B, hidden_size, _, _ = idims

    W_out = init_dict['l5_output_weights']
    B_out = init_dict['l5_output_bias']

    dense = network.add_fully_connected(input_tensor, B_out.size, W_out, B_out)

    OUT = network.add_shuffle(dense.get_output(0))
    OUT.second_transpose = (1, 0, 2, 3, 4)
    return OUT


def pooled_classfier_output(config, init_dict, network, input_tensor, **kwargs):
    """
    Create the pooled classfier over sequence output
    """

    idims = input_tensor.shape
    assert len(idims) == 5
    S, B, hidden_size, _, _ = idims

    W_pooler = init_dict['bert_pooler_dense_kernel']
    B_pooler = init_dict['bert_pooler_dense_bias']

    pooler = network.add_fully_connected(input_tensor, B_pooler.size, W_pooler, B_pooler)

    X_tanh = network.add_activation(pooler.get_output(0), trt.ActivationType.TANH)
    X_tanh_tensor = X_tanh.get_output(0)

    W_out = init_dict['output_weights']
    B_out = init_dict['output_bias']

    dense = network.add_fully_connected(X_tanh_tensor, B_out.size, W_out, B_out)

    OUT = network.add_shuffle(dense.get_output(0))
    OUT.second_transpose = (1, 0, 2, 3, 4)
    return OUT

def pooled_classfier_output_fast(config, init_dict, network, input_tensor, **kwargs):
    """
    Create the pooled classfier over sequence output
    """

    idims = input_tensor.shape
    assert len(idims) == 5
    S, B, hidden_size, _, _ = idims

    W_pooler = init_dict['l5_pooler_dense_kernel']
    B_pooler = init_dict['l5_pooler_dense_bias']

    pooler = network.add_fully_connected(input_tensor, B_pooler.size, W_pooler, B_pooler)

    X_tanh = network.add_activation(pooler.get_output(0), trt.ActivationType.TANH)
    X_tanh_tensor = X_tanh.get_output(0)

    W_out = init_dict['l5_output_weights']
    B_out = init_dict['l5_output_bias']

    dense = network.add_fully_connected(X_tanh_tensor, B_out.size, W_out, B_out)

    OUT = network.add_shuffle(dense.get_output(0))
    OUT.second_transpose = (1, 0, 2, 3, 4)
    return OUT
