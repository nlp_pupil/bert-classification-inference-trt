# 将SavedModel转换成TensorRT Engine

## 转TensorRT步骤
如果还没有配置环境，先参考下方【环境配置】。

环境配置好后，先执行`export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/Desktop/TensorRT-8.0.1.6/lib`。

进入`build_engine/`目录，运行:
```bash
python builder_classifier.py -m ~/Desktop/saved_model/variables/variables 
```
其中，`~/Desktop/saved_model`是用bert-train训练得到的SavedModel目录，内容形如：
```bash
/root/Desktop/saved_model/:
assets  id2label.json  keras_metadata.pb  saved_model.pb  variables

/root/Desktop/saved_model/assets:
vocab.txt

/root/Desktop/saved_model/variables:
variable2weight.json  variables.data-00000-of-00001  variables.index
```

其他参数解释：
- -o, 输出engine文件的路径，暂时必须使用默认值，即`../inference/resources/my_model.engine`。
- -b, batch size，暂时必须使用默认值，即1。
- -s, max_length，暂时必须使用默认值，即128。
- -c, 暂时必须使用默认值，即`./resources/`
- -f, 是否使用fp16精度，暂时必须使用默认值，即True。
- -i, 暂时必须使用默认值，即False。
- -t, 暂时必须使用默认值，即False。
- -w, 使用默认值即可。
- -g, 暂时必须使用默认值，即False。
- -iln, 暂时必须使用默认值，即False。
- -imh, 暂时必须使用默认值，即False。
- --output-function, 暂时必须使用默认值，即pooled_classfier_output。
- --weights-mapping-file, 暂时必须使用默认值，即`./resources/weights_mapping.json`。
- --squeeze, 暂时必须使用默认值，即False。

参考：
- https://github.com/NVIDIA/TensorRT/tree/main/demo/BERT


## 环境配置
建议使用docker。如果使用docker，后面的就不用看了。

|环境|版本|查看方式|
|-|-|-|
|CUDA驱动|11.4|nvidia-smi|
|CUDA|11.3|nvcc --version|
|cuDNN|8.2.0|whereis cudnn_version.h, cat /usr/include/cudnn_version.h \| grep CUDNN_MAJOR -A 2|
|显卡|RTX 2090||

安装TensorRT步骤（参考[4.6. Tar File Installation](https://docs.nvidia.com/deeplearning/tensorrt/install-guide/index.html#installing-tar)）：
1. 打开https://developer.nvidia.com/nvidia-tensorrt-8x-download， 点击下载[TensorRT 8.0.1 GA for Linux x86_64 and CUDA 11.3 TAR package](https://developer.nvidia.com/compute/machine-learning/tensorrt/secure/8.0.1/tars/tensorrt-8.0.1.6.linux.x86_64-gnu.cuda-11.3.cudnn8.2.tar.gz) ，其他版本可能也可以。注意，机器上的CUDA版本必须跟所下载压缩包的文件名上的CUDA版本兼容（最好一样）。

2. 解压下载好的压缩包。`tar xzvf TensorRT-${version}.Linux.${arch}-gnu.${cuda}.${cudnn}.tar.gz`，得到`TensorRT-${version}`文件夹，如：

    ```bash
    $ ls TensorRT-8.0.1.6
    bin  data  doc  graphsurgeon  include  lib  onnx_graphsurgeon  python  samples  targets  TensorRT-Release-Notes.pdf  uff
    ```

3. 将TensorRTlib 目录 加入到环境变量 LD_LIBRARY_PATH:

    ```bash
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/Desktop/TensorRT-8.0.1.6/lib
    ```

4. Install the Python TensorRT wheel file

    ```bash
    cd TensorRT-8.0.1.6/python/
    # 根据python版本选择相应的轮子
    python3 -m pip install tensorrt-*-cp3x-none-linux_x86_64.whl
    ```

5. 验证。进入python解释器，运行`import tensorrt`，不报错说明安装成功。

补充说明：
- 转TensorRT Engine需要安装TensorRT，环境配置的最终目的是安装TensorRT。TensorRT有很多版本，安装其他版本也可以，只要和CUDA驱动版本、CUDA版本、cuDNN版本兼容即可。
- 再次建议使用docker，免除繁琐的各种版本配置。





