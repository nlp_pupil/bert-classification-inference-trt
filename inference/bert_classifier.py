import threading
import json
from collections import namedtuple
import tensorflow as tf
import numpy as np

import forward
from bert_preprocess import BertPreprocessor


Label = namedtuple('Label', ['label_name', 'score'])


def synchronized(func):
    func.__lock__ = threading.Lock()

    def synced_func(*args, **kwargs):
        with func.__lock__:
            return func(*args, **kwargs)

    return synced_func


class BertMultiClassClassifier():
    def __init__(self,engine_name='my_model.engine'):
        self.batch_size = 1
        self.seq_len = 128
        self.tf_engine = forward.TfEngine()
        self.tf_engine.load('resources/'+engine_name)
        self.preprocessor = BertPreprocessor()
        self.id2label = self.load_id2label()

        
    def load_id2label(self):
        id2label_file_path = 'resources/id2label.json'
        return json.load(open(id2label_file_path))


    @synchronized 
    def classify(self, text: str) -> Label:
        input = self.preprocessor.preprocess([text])
        inputs = input
        output_dict = self.tf_engine.forward(inputs)
        output_value = next(iter(output_dict.values()))
        pred = output_value[0][0]
        pred = np.squeeze(pred)
        label_id = np.argmax(pred)
        pred = tf.nn.softmax(pred)
        score = np.amax(pred)
        label = self.id2label[str(label_id)]
        label = Label(label_name=label,score=score)
        return label






