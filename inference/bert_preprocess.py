from tokenizers import BertWordPieceTokenizer
import  numpy as np

class BertPreprocessor():
    def __init__(self):
        self.seq_len = 128
        self.make_tokenizer()

    def make_tokenizer(self):
        vocab_file_path = 'resources/bert_vocab.txt'
        self.tokenizer = BertWordPieceTokenizer(vocab_file_path, lowercase=True)
        self.tokenizer.enable_padding(length=self.seq_len)

    def preprocess(self,texts:[str]):
        output = self.tokenizer.encode(texts[0])  # 预测的时候都是单条

        input_word_ids = self.make_array(output.ids[:self.seq_len])
        input_mask = self.make_array(output.attention_mask[:self.seq_len])
        input_type_ids = self.make_array(output.type_ids[:self.seq_len])

        return {
            'input_ids': input_word_ids,
            'input_mask': input_mask,
            'segment_ids': input_type_ids
        }

    def make_array(self,ids:[int]):
        # 模型输入需要batch，所以加了一层括号
        # TensorRT要求的形状是(seq_len, batch_size)，所以加转置
        return np.transpose(np.array([ids], dtype=np.int32))



