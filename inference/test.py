from bert_classifier import BertMultiClassClassifier

import time

if __name__ == '__main__':
    import sys
    filename = sys.argv[1]
    classifier = BertMultiClassClassifier()

    with open(filename) as f:
        i = 0
        correct_num = 0

        # 预热
        for _ in range(1000):
            classifier.classify('哈哈哈')

        start_time = time.time()
        for line in f:
            i += 1
            fields = line.strip().split('\t')
            if not len(fields)==2:
                continue
            label,text = fields
            pred = classifier.classify(text)
            if i < 10:
                print ('文本内容:{0}'.format(text))
                print ('预测标签:{0}'.format(pred))
                print ('真实标签:{0}'.format(label))
                print ()

            if pred.label_name==label:
                correct_num+=1

        print ('准确率: {0:.2f}%'.format(100*correct_num/i))
        print ('平均每条文本耗时: {0:.2f}毫秒'.format(1000*(time.time()-start_time)/i))
        #print("--- %s millieseconds/sentence ---" % ((time.time() - start_time)*1000/i))



