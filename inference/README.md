# 基于TensorRT进行BERT分类模型推理

## 推理
如果没配置环境，先参考【环境配置】。

推理之前，先确定构建了新的engine。并且，`./resources/id2label.json`复制自`~/Desktop/saved_model/id2label.json`。

推理的核心代码很简单，如下：
```python
classifier = BertMultiClassClassifier()
label = classifier.classify('高铁概念股大涨 南北二车快速冲高。')
```
详见`test.py`。



如果出现以下报错：
```bash
Could not load dynamic library 'libcudart.so.10.0'
```
解决办法是执行
```bash
conda install cudatoolkit=10.0
```


## 环境配置
推理除了需要TensorRT，还需要[Forward](https://github.com/Tencent/Forward)。Forward是腾讯开发的深度学习推理加速框架，这里使用Forward的原因是它封装了一些繁琐的细节。
1. 在其他目录下，比如`Desktop/`。`git clone https://github.com/Tencent/Forward.git`

2. 下载 `Tensorflow 1.15.0`

    ```bash
    cd Forward/source/third_party/tensorflow/
    wget https://github.com/neargye-forks/tensorflow/releases/download/v1.15.0/libtensorflow-gpu-linux-x86_64-1.15.0.tar.gz
    tar -xvf libtensorflow-gpu-linux-x86_64-1.15.0.tar.gz
    ```

    如果wget连不上，多执行几次，或手动打开链接下载。

3. 创建 `build` 文件夹。

    ```bash
    cd ~/Desktop/Forward/
    rm -rf build
    mkdir -p build
    cd build/
    ```
4. [安装cmake3.13](https://gist.github.com/gojimmypi/615f4ff363592fd743fa1e04bd0127cb)。
    ```bash
    cd ~/workspace/
    
    sudo apt-get install build-essential
    wget http://www.cmake.org/files/v3.13/cmake-3.13.3.tar.gz
    tar xf cmake-3.13.3.tar.gz
    cd cmake-3.13.3
    ./configure
    make
    ```
5. 进入`~/Desktop/Forward/build`目录，使用 `cmake` 生成构建关系。

    ```bash
    ~/Desktop/cmake-3.13.3/bin/cmake ..  -DTensorRT_ROOT=~/Desktop/TensorRT-8.0.1.6/ -DENABLE_TENSORFLOW=ON -DENABLE_UNIT_TESTS=ON -DBUILD_PYTHON_LIB=ON -DPYTHON_EXECUTABLE=/root/miniconda3/bin/python
    # PYTHON_EXECUTABLE 可通过which python查看
    ```

    以下报错可忽略：

    ```bash
    Traceback (most recent call last):
      File "<string>", line 1, in <module>
    ModuleNotFoundError: No module named 'tensorflow'
    Traceback (most recent call last):
      File "<string>", line 1, in <module>
    ModuleNotFoundError: No module named 'tensorflow'
    -- TF_PYLIB_DIR =  
    CMake Warning at CMakeLists.txt:326 (message):
      _pywrap_tensorflow_internal.so NOT-FOUND in the python-packages! Use
      libtensorflow.so under the source/third_party/tensorflow/lib.  In this
      case, 'import forward' cannot be used with 'import tensorflow' in the same
      time.
    
    ```

5. 使用 make 构建项目

    ```
    make -j
    ```

6. 运行 unit_test 验证项目是否构建成功。

    ```
    cd bin/
    ./unit_test --gtest_filter=TestTfNodes.*
    ```

7. 进入python解释器验证。

    ```python
    import forward
    ```

    如果有以下报错，可忽略。

    ```bash
    ASSERTION FAILURE FROM EASYLOGGING++ (LINE: 2590) [(assertionPassed = base::utils::File::pathExists(configurationFile.c_str(), true))] WITH MESSAGE "Configuration file [forward_log.conf] does not exist!"
    ```
8. 到此，`Forward/build/bin`目录下新增个`forward.cpython-38-x86_64-linux-gnu.so`文件。推理时需要这个文件，将它移到`inference/`目录。
    ```
    mv ~/Desktop/Farword/build/bin/forward.cpython-38-x86_64-linux-gnu.so inference/
    ```