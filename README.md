# 基于TensorRT的BERT分类模型推理

本代码库与[bert-classification-train-tf2](https://gitee.com/nlp_pupil/bert-classification-train-tf2)（简称bert-train）搭配使用，bert-train负责训练BERT分类模型，本代码库负责推理。

## 步骤
1. 用bert-train训练模型，得到SavedModel文件夹，命名为`saved_model/`。
2. 用`build_engine/`目录将`saved_model/`转换为TensorRT Engine文件，记为`my_model.engine`。具体步骤见`build_engine/README.md`。
3. 基于`my_model.engine`，用`inference/`目录进行推理。具体步骤见`inference/README.md`。

